# Build Procedure
* install node modules at root directory.
     * `npm install`
* start node server
     * `node index.js`
* install node modules in `./ui/`
     * cd ./ui/
     * npm install
* build bundle
     * npm run build
* open a browser and goto `localhost:8000`